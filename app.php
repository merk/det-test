<?php
/**
 * Created by PhpStorm.
 * Author: Evgeny Merkushev <byMerk@live.ru>
 * Date: 12.05.17 16:26
 */

use Phalcon\Di;

try {

	require "vendor/autoload.php";
	$config = include "configs/config.php";

	if (empty($config)) {
		throw new \Exception("Can't find config");
	}

	$di = new Di();

	$di->setShared('config', $config);

	$di->setShared('db', function () use ($config) {
		$dbConfig = $config->database->toArray();
		return new Phalcon\Db\Adapter\Pdo\Mysql($dbConfig);
	});


	$capture = new \Capture\Capture($config->stream->url, $di);
	$capture->run();



} catch (\Exception $e)
{
	echo  $e->getMessage();
}



