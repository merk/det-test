<?php
/**
 * Created by PhpStorm.
 * Author: Evgeny Merkushev <byMerk@live.ru>
 * Date: 12.05.17 16:27
 */


return new \Phalcon\Config([

	'ffmpeg' => [
		'bin' => '/usr/local/bin/ffmpeg'
	],

    'stream' => [
    	'url' => ''
    ],

    'database' => [
		'host'        => '',
		'username'    => '',
		'password'    => '',
		'dbname'      => '',
		'charset'     => 'utf8',
	]

]);