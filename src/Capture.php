<?php
/**
 * Created by PhpStorm.
 * Author: Evgeny Merkushev <byMerk@live.ru>
 * Date: 14.05.17 18:24
 */

namespace Capture;

use \Symfony\Component\Process\Process;
use Phalcon\DiInterface;


class Capture extends Component {

	/**
	 * @var string
	 */
	private $_command;

	/**
	 * Duration for once capture video file
	 *
	 * @var int (seconds)
	 */
	private $_captureDuration = 60 * 60;

	/**
	 * Time for safe pre starting new capture;
	 *
	 * @var int
	 */
	private $_safeStartTime = 5 * 60;

	/**
	 * @var int
	 */
	private $_countCapture = 0;


	/**
	 * @var Schedule
	 */
	protected $schedule;

	/**
	 * @var Item\Item
	 */
	protected $captureItem;

	/**
	 * @var string
	 */
	private $_currentFileName;

	/**
	 * Capture constructor.
	 *
	 * @param $streamUrl
	 * @param DiInterface $di
	 */
	function __construct(string $streamUrl, DiInterface $di)
	{

		parent::__construct($di);

		$this->_command = $this->getDI()->getShared('config')->ffmpeg->bin
		                  . ' -i ' . $streamUrl
		                  . ' –c copy –f mp4 –t  ' . $this->_captureDuration;

		$this->schedule = new Schedule($di);
		$this->captureItem = new Item\Item($di);
	}


	/**
	 * Starting video capture
	 *
	 * @param $process
	 */
	public function run($process = null)
	{

		if ( $process === null ) {
			$process = $this->_newProcess();
		}

		$secondProcess = false;

		foreach ($process as $type => $buffer) {

			if ( $process::ERR === $type ) {

				$duration = $this->_getCaptureSeconds( $buffer );

				if (!$secondProcess && $duration !== false && $duration >= ( $this->_captureDuration - $this->_safeStartTime ))
				{
					$secondProcess = $this->_newProcess();
				}

			}
		}

		$this->run($secondProcess);
	}


	/**
	 * Return current capture duration
	 *
	 * @param $bufferString
	 *
	 * @return bool
	 */
	private function _getCaptureSeconds($bufferString)
	{

		$pattern = '/^[A-z. :]*(\d\d\d:\d\d:\d\d)/';

		preg_match($pattern, $bufferString, $matches);

		if (!empty($matches[1])) {

			$timeArr = explode(':', $matches[1]);

			$timeArr = array_map('intval', $timeArr);

			return $timeArr[1] * 3600 + $timeArr[1] * 60 + $timeArr[2];

		}

		return false;
	}


	/**
	 * Return Capture name
	 *
	 * @return string
	 */
	private function _getNewName()
	{
		$this->_countCapture++;

		return $this->_countCapture . '_video.mp4';
	}


	/**
	 * @return bool
	 */
	public function addInfo()
	{
		$captureId = $this->captureItem->addItem($this->_currentFileName);

		if (empty($captureId)) {
			return false;
		}

		$this->schedule->set($captureId);
	}


	/**
	 * @return string
	 */
	private function _getCommand()
	{
		$this->_currentFileName = $this->_getNewName();
		return $this->_command . $this->_currentFileName;
	}

	/**
	 * create new process
	 */
	private function _newProcess()
	{
		$command = $this->_getCommand();

		$process = new Process($command);
		$process->start(); // async
		$this->addInfo(); // async

		return $process;
	}


}