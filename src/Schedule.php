<?php
/**
 * Created by PhpStorm.
 * Author: Evgeny Merkushev <byMerk@live.ru>
 * Date: 14.05.17 22:25
 */

namespace Capture;


class Schedule extends Component {


	/**
	 * @var string
	 */
	private $_table = 'schedule';


	public function set(int $captureId)
	{
		$this->di->getShared("db");


		$fork = new \duncan3dc\Forker\Fork;

		$fork->call(function () use ($captureId) {

			$datetime = new \DateTime();

			$timePattern = 'Y-m-d H:i';

			$client = new \SoapClient("https://localhost/WebServices/Schedule.asmx?WSDL");

			$schedule = $client->getInfoByTime([
				'start_time'     => $datetime->format($timePattern),
				'stop_time'     => $datetime->modify('+1 hours')->format($timePattern)
			]);

			$sql = 'INSERT INTO . ' . $this->_table . ' (`capture_id`, `name`, `start_time`, `stop_time`) VALUES(?,?,?,?)';

			foreach ($schedule as $row)
			{
				/**
				 * @todo set query
				 */
				$this->getDb()->query()->fetchAll();
			}


		});


	}


}